#define RELAY_2 7
#define MINUTES 60000

void setup() {
  // put your setup code here, to run once:
  pinMode(RELAY_2, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(RELAY_2, HIGH);
  delay(5 * MINUTES);
  digitalWrite(RELAY_2, LOW);
  delay(25 * MINUTES);
}
