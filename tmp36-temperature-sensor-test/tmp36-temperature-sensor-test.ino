#define TMP36_PIN A1

void setup() {
  Serial.begin(115200);
  analogReference(DEFAULT);

  // Dump first ADC value
  analogRead(TMP36_PIN);
  delay(10);
}

int analogReadWithRef(int pin, int ref)
{
  // Inspired by: https://forum.arduino.cc/index.php?topic=613867.0
  analogReference(ref);
  delay(10);
  analogRead(pin);
  delay(8);
  return analogRead(pin);
}

void loop() {
  int sensorVal = analogReadWithRef(TMP36_PIN, INTERNAL);
  Serial.print("Sensor Value: ");
  Serial.print(sensorVal);

  float voltage = (sensorVal / 1024.0) * 1.1;
  Serial.print(", Voltage(V): ");
  Serial.print(voltage);

  float temperature_c = (voltage - 0.5) * 100;
  Serial.print(", Temp(C): ");
  Serial.print(temperature_c);

  float temperature_f = (temperature_c * 1.8) + 32.0;
  Serial.print(" , Temp(F): ");
  Serial.println(temperature_f);

  delay(1000);
}
