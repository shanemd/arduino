#define RELAY_1 4
#define RELAY_2 7
#define RELAY_3 8
#define RELAY_4 12

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(RELAY_1, OUTPUT);
  pinMode(RELAY_2, OUTPUT);
  pinMode(RELAY_3, OUTPUT);
  pinMode(RELAY_4, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("All off");
  digitalWrite(RELAY_1, LOW);
  digitalWrite(RELAY_2, LOW);
  digitalWrite(RELAY_3, LOW);
  digitalWrite(RELAY_4, LOW);
  delay(1000);
  Serial.println("1");
  digitalWrite(RELAY_1, HIGH);
  delay(1000);
  Serial.println("2");
  digitalWrite(RELAY_1, LOW);
  digitalWrite(RELAY_2, HIGH);
  delay(1000);
  Serial.println("3");
  digitalWrite(RELAY_2, LOW);
  digitalWrite(RELAY_3, HIGH);
  delay(1000);
  Serial.println("4");
  digitalWrite(RELAY_3, LOW);
  digitalWrite(RELAY_4, HIGH);
  delay(1000);
  Serial.println("All off");
  digitalWrite(RELAY_4, LOW);
  delay(1000);
  Serial.println("All on");
  digitalWrite(RELAY_1, HIGH);
  digitalWrite(RELAY_2, HIGH);
  digitalWrite(RELAY_3, HIGH);
  digitalWrite(RELAY_4, HIGH);
  delay(1000);
}
