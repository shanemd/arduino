#define BTN    11
#define GREEN  10
#define YELLOW  9
#define RED     6

int switchState = 0;

void setup() {
  pinMode(BTN,    INPUT);
  pinMode(GREEN,  OUTPUT);
  pinMode(YELLOW, OUTPUT);
  pinMode(RED,    OUTPUT);
}

void loop() {
  switchState = digitalRead(BTN);

  if (switchState == LOW) {
    digitalWrite(GREEN,  HIGH);
    digitalWrite(YELLOW, LOW);
    digitalWrite(RED,    LOW);
  } else {
    digitalWrite(GREEN,  LOW);
    digitalWrite(YELLOW, HIGH);
    digitalWrite(RED,    LOW);

    delay(250);

    digitalWrite(GREEN,  LOW);
    digitalWrite(YELLOW, LOW);
    digitalWrite(RED,    HIGH);

    delay(250);
  }

}
