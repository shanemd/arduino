#define RELAY_1 4
#define RELAY_2 7
#define RELAY_3 8
#define RELAY_4 12

#include <LiquidCrystal.h>
#define LCD_RS  5
#define LCD_EN  6
#define LCD_D4  9
#define LCD_D5 10
#define LCD_D6 11
#define LCD_D7 13

#define MINUTES 60000
#define SECONDS  1000

#define FAN_RELAY  RELAY_2
#define BTN        3
#define BTN_DEBOUNCE_MILLIS 300

#define FAN_ON_MILLIS   10 * MINUTES
#define FAN_OFF_MILLIS  50 * MINUTES

LiquidCrystal lcd(LCD_RS, LCD_EN, LCD_D4, LCD_D5, LCD_D6, LCD_D7);

unsigned long beginMillis = 0;
bool fanOn = false;

byte backslash[8] = {
  B00000,
  B10000,
  B01000,
  B00100,
  B00010,
  B00001,
  B00000,
  B00000
};

const char fanIcon[] = {'-', byte(0), '|', '/'};
int fanN = 4;
int fanI = 0;
int fanCounter = 0;

void startFan()
{
  digitalWrite(FAN_RELAY, HIGH);
  beginMillis = millis();
  fanOn = true;

  lcd.setCursor(0, 0);
  lcd.print("Fan ON ");

  fanCounter = 0;
  fanI = 0;
}

void stopFan()
{
  digitalWrite(FAN_RELAY, LOW);
  beginMillis = millis();
  fanOn = false;

  lcd.setCursor(0, 0);
  lcd.print("Fan OFF");
}

void onButton()
{
  if ((millis() - beginMillis) < BTN_DEBOUNCE_MILLIS) {
    return;
  }
  
  if (!fanOn) {
    startFan();
  } else {
    stopFan();
  }
}

void setup() {
  pinMode(FAN_RELAY, OUTPUT);
  pinMode(BTN, INPUT);
  attachInterrupt(digitalPinToInterrupt(BTN), onButton, RISING);
  lcd.createChar(0, backslash);
  lcd.begin(16, 2);
  startFan();
}

void loop() {
  unsigned long now = millis();

  if (fanOn) {
    if ((now - beginMillis) > FAN_ON_MILLIS) {
      stopFan();
    }
  } else {
    if ((now - beginMillis) > FAN_OFF_MILLIS) {
      startFan();
    }
  }

  lcd.setCursor(0, 1);
  lcd.print("for ");
  if (fanOn) {
    lcd.print(((FAN_ON_MILLIS - (now - beginMillis)) / MINUTES) + 1);
  } else {
    lcd.print(((FAN_OFF_MILLIS - (now - beginMillis)) / MINUTES) + 1);
  }
  lcd.print(" minutes  ");

  //Fan icon
  lcd.setCursor(15, 0);
  if (fanOn) {
    if (fanCounter == 2) {
      fanI++;
      if (fanI == fanN) {
        fanI = 0;
      }
      fanCounter = 0;
    } else {
      fanCounter++;
    }
    lcd.write(fanIcon[fanI]);
  } else {
    lcd.write(fanIcon[0]);
  }

  delay(200);
}
