#define RELAY_1 4
#define RELAY_2 7
#define RELAY_3 8
#define RELAY_4 12
#define MINUTES 60000
#define SECONDS  1000

#define FAN_RELAY  RELAY_2
#define BTN        3
#define BTN_DEBOUNCE_MILLIS 300

#define FAN_ON_MILLIS   10 * MINUTES
#define FAN_OFF_MILLIS  50 * MINUTES

unsigned long beginMillis = 0;
bool fanOn = false;

void startFan()
{
  digitalWrite(FAN_RELAY, HIGH);
  beginMillis = millis();
  fanOn = true;
}

void stopFan()
{
  digitalWrite(FAN_RELAY, LOW);
  beginMillis = millis();
  fanOn = false;
}

void onButton()
{
  if ((millis() - beginMillis) < BTN_DEBOUNCE_MILLIS) {
    return;
  }
  
  if (!fanOn) {
    startFan();
  } else {
    stopFan();
  }
}

void setup() {
  pinMode(FAN_RELAY, OUTPUT);
  pinMode(BTN, INPUT);
  attachInterrupt(digitalPinToInterrupt(BTN), onButton, RISING);
  startFan();
}

void loop() {
  unsigned long now = millis();

  if (fanOn) {
    if ((now - beginMillis) > FAN_ON_MILLIS) {
      stopFan();
    }
  } else {
    if ((now - beginMillis) > FAN_OFF_MILLIS) {
      startFan();
    }
  }

  delay(200);
}
