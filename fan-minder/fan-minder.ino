#include <RollingAverage.h>
#include <LiquidCrystal.h>
#include "RTClib.h"

#define MINUTES 60000
#define SECONDS  1000

// Pin assignments
#define RELAY_1      4
#define RELAY_2      7
#define RELAY_3      8
#define RELAY_4      12
#define LCD_RS       5
#define LCD_EN       6
#define LCD_D4       9
#define LCD_D5       10
#define LCD_D6       11
#define LCD_D7       13
#define BACKLIGHT    2
#define FAN_RELAY    RELAY_2
#define BTN          3
#define MINUTE_WIPER A0
#define TEMP_SENSOR  A1

// Application parameters
#define BACKLIGHT_TIMEOUT      10 * SECONDS
#define BACKLIGHT_WIPER_THRESH 100
#define BTN_DEBOUNCE_MILLIS    350
#define FAN_PERIOD_MINUTES     60
#define TEMP_OFFSET            3.0
#define TEMP_AVERAGE_LENGTH    20
#define TEMP_CHANGE_RELUCTANCE 0.3

// Constants
const byte backslash[8] = {
  B00000,
  B10000,
  B01000,
  B00100,
  B00010,
  B00001,
  B00000,
  B00000
};
const char fanIcon[] = {'-', byte(0), '|', '/'};
const int  fanN      = sizeof(fanIcon) / sizeof(char);

// Peripherals
LiquidCrystal lcd(LCD_RS, LCD_EN, LCD_D4, LCD_D5, LCD_D6, LCD_D7);
RTC_DS1307    rtc;

// Application state variables
unsigned long  fan_on_millis       = 10 * MINUTES;
unsigned long  fan_off_millis      = 50 * MINUTES;
unsigned long  beginMillis         = 0;
bool           fanOn               = false;
unsigned long  backlightMillis     = 0;
bool           backlightOn         = false;
float          temp_raw_f          = 0.0;
float          temp_average_f      = 0.0;
int            temp_rounded_f      = 0;
int            fanI                = 0;
int            fanCounter          = 0;
unsigned long  lastPressMillis     = 0;
RollingAverage temp_average_filter = RollingAverage(TEMP_AVERAGE_LENGTH);
DateTime       rtc_now;

int round_with_reluctance(float val, float reluctance, int prev_return)
{
  if (floor(val) == prev_return) {
    return prev_return;
  }
  if (floor(val) > prev_return) {
    return floor(val - reluctance);
  }
  if (floor(val) < prev_return) {
    return floor(val + reluctance);
  }
}

void start_fan()
{
  digitalWrite(FAN_RELAY, HIGH);
  beginMillis = millis();
  fanOn = true;

  fanCounter = 0;
  fanI = 0;
}

void stop_fan()
{
  digitalWrite(FAN_RELAY, LOW);
  beginMillis = millis();
  fanOn = false;
}

void wake_backlight()
{
  backlightMillis = millis();
  if (!backlightOn) {
    digitalWrite(BACKLIGHT, HIGH);
    backlightOn = true;
  }
}

void on_button()
{
  if ((millis() - lastPressMillis) < BTN_DEBOUNCE_MILLIS) {
    return;
  }
  lastPressMillis = millis();

  if (!backlightOn) {
    wake_backlight();
    return;
  } else {
    if (!fanOn) {
      start_fan();
    } else {
      stop_fan();
    }
    wake_backlight();
  }
}

int analog_read_with_ref(int pin, int ref)
{
  // Inspired by: https://forum.arduino.cc/index.php?topic=613867.0
  analogReference(ref);
  delay(10);
  analogRead(pin);
  delay(10);
  return analogRead(pin);
}

int get_minutes_from_wiper(int wiper_pin, int range_in_minutes)
{
  unsigned long adc = analogRead(wiper_pin);

  return (int)((adc * range_in_minutes) / 1023);
}

void update_fan_onoff_values(int minutes_on, int fan_period_minutes, unsigned long *on_duration_millis, unsigned long *off_duration_millis)
{
  *on_duration_millis = minutes_on * MINUTES;
  *off_duration_millis = (fan_period_minutes - minutes_on) * MINUTES;
}

float get_temperature_f(int pin, int ref, float scale)
{
  int sensorVal = analogRead(pin);
  float voltage = (sensorVal / 1024.0) * scale;
  float temperature_c = (voltage - 0.5) * 100;
  float temperature_f = (temperature_c * 1.8) + 32.0;

  return temperature_f + TEMP_OFFSET;
}

void update_display()
{
  unsigned long now = millis();

  // XXX: Make these prints lazy (store previous values and only
  //      write if changes occurred.

  // Top left quadrant - "Fan  XXm" or "Wait XXm" or "Fan OFF "
  lcd.setCursor(0, 0);

  if (fan_on_millis > 0 && fan_off_millis > 0) {
    int mins;
    if (fanOn) {
      lcd.print("Fan  ");
      mins = ((fan_on_millis - (now - beginMillis)) / MINUTES) + 1;
    } else {
      lcd.print("Wait ");
      mins = ((fan_off_millis - (now - beginMillis)) / MINUTES) + 1;
    }

    if (mins < 10) {
      lcd.print(" ");
    }
    lcd.print(mins);
    lcd.print("m");
  } else {
    lcd.print("        ");
  }

  // Top right quadrant

  // Current temperature - " XXX F "
  lcd.setCursor(8, 0);
  lcd.print(" ");
  if (temp_rounded_f < 100)
    lcd.print(" ");
  if (temp_rounded_f < 10)
    lcd.print(" ");
  lcd.print(temp_rounded_f);
  lcd.print(" F ");

  // Fan icon - "/" or "-" or "\" or "|"
  lcd.setCursor(15, 0);
  if (fanOn) {
    if (fanCounter == 2) {
      fanI++;
      if (fanI == fanN) {
        fanI = 0;
      }
      fanCounter = 0;
    } else {
      fanCounter++;
    }
    lcd.write(fanIcon[fanI]);
  } else {
    lcd.write(fanIcon[0]);
  }

  // Bottom left quadrant - "HH:MMpm "
  lcd.setCursor(0, 1);
  int hour = rtc_now.hour();
  int minute = rtc_now.minute();
  bool pm = false;
  if (hour > 12) {
    hour -= 12;
    pm = true;
  }
  if (hour == 0) {
    hour = 12;
  }

  if (hour < 10) {
    lcd.print(" ");
  }
  lcd.print(hour);
  lcd.print(":");
  if (minute < 10) {
    lcd.print("0");
  }
  lcd.print(minute);
  if (pm) {
    lcd.print("pm");
  } else {
    lcd.print("am");
  }

  // Bottom right quadrant
  lcd.setCursor(10, 1);
  lcd.print(String(fan_on_millis / MINUTES));
  lcd.print("m/");
  if (FAN_PERIOD_MINUTES == 60) {
    lcd.print("hr ");
  } else {
    lcd.print(String(FAN_PERIOD_MINUTES));
    lcd.print("m");
  }
}

void setup() {
  Serial.begin(115200);
  analogReference(EXTERNAL);
  pinMode(BACKLIGHT, OUTPUT);
  pinMode(FAN_RELAY, OUTPUT);
  pinMode(BTN, INPUT);
  lcd.createChar(0, backslash);
  lcd.begin(16, 2);

  attachInterrupt(digitalPinToInterrupt(BTN), on_button, RISING);

  temp_raw_f     = get_temperature_f(TEMP_SENSOR, EXTERNAL, 3.3);
  temp_average_filter.add(temp_raw_f);
  temp_average_f = temp_raw_f;
  temp_rounded_f = floor(temp_raw_f);

  int minutes_on = get_minutes_from_wiper(MINUTE_WIPER, FAN_PERIOD_MINUTES);
  update_fan_onoff_values(minutes_on, FAN_PERIOD_MINUTES, &fan_on_millis, &fan_off_millis);

  stop_fan();
  wake_backlight();

  if (!rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1);
  }
  if (!rtc.isrunning()) {
    Serial.println("RTC isn't set. Setting RTC time to compile time.");
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
  rtc_now = rtc.now();

  update_display();
}

void loop() {
  static unsigned long counter = 0;
  static unsigned long last_now = 0;
  unsigned long now = millis();

  // 200 ms loop
  if (1) {

    unsigned long last_fan_on_millis = fan_on_millis;
    int minutes_on = get_minutes_from_wiper(MINUTE_WIPER, FAN_PERIOD_MINUTES);
    update_fan_onoff_values(minutes_on, FAN_PERIOD_MINUTES, &fan_on_millis, &fan_off_millis);

    if (last_fan_on_millis != fan_on_millis) {
      update_display();
      wake_backlight();
    }

    if (fanOn) {
      if ((now - beginMillis) > fan_on_millis) {
        stop_fan();
      }
    } else {
      if ((now - beginMillis) > fan_off_millis) {
        start_fan();
      }
    }

    temp_raw_f = get_temperature_f(TEMP_SENSOR, EXTERNAL, 3.3);
    temp_average_filter.add(temp_raw_f);
    if (temp_average_filter.is_full()) {
      temp_average_f = temp_average_filter.get_avg();
      temp_rounded_f = round_with_reluctance(temp_average_f, TEMP_CHANGE_RELUCTANCE, temp_rounded_f);
    }

    if (backlightOn && ((millis() - backlightMillis) >= BACKLIGHT_TIMEOUT)) {
      digitalWrite(BACKLIGHT, LOW);
      backlightOn = false;
    }
  }

  // 1s loop
  if (counter % 5 == 0) {

    rtc_now = rtc.now();

    update_display();

    Serial.print(now);
    Serial.print(",");
    Serial.print(temp_raw_f);
    Serial.print(",");
    Serial.print(temp_average_f);
    Serial.print(",");
    Serial.println(temp_rounded_f);

    Serial.print("Loop time: ");
    Serial.println(millis() - now);

    Serial.print(rtc_now.hour());
    Serial.print(" ");
    Serial.println(rtc_now.minute());
  }

  counter++;
  counter %= 300; // 1 minute rollover
  delay(200 - (millis() - now));
}
