#include <RTClib.h>

RTC_DS1307 rtc;

// Note: The output of this sketch when the RTC gets disconnected is:
// 2165-165-165  165:165:85


void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  while (!rtc.begin()) {
      Serial.println("Couldn't find RTC");
      delay(5000);
  }

  if (!rtc.isrunning()) {
    while (1) {
      Serial.println("RTC is not running. Please flash time-set code to start the RTC.");
      delay(5000);
    }
    
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  DateTime now = rtc.now();

  Serial.print(now.year());
  Serial.print("-");
  Serial.print(now.month());
  Serial.print("-");
  Serial.print(now.day());
  Serial.print("  ");
  Serial.print(now.hour());
  Serial.print(":");
  Serial.print(now.minute());
  Serial.print(":");
  Serial.print(now.second());
  Serial.println("");

  delay(1000);
}
