class RollingAverage
{
  public:
    RollingAverage(int length) {
      buf_len = length;
      buf = calloc(buf_len, sizeof(float));
      next_idx = 0;
      cur_size = 0;
    };

    void add(float value) {
      buf[next_idx] = value;
      next_idx = (next_idx + 1) % buf_len;
      if (cur_size != buf_len)
        cur_size++;
    };

    float get_avg() {
      float sum = 0.0;
      for (int i = 0; i < cur_size; i++) {
        sum += buf[i];
      }
      return sum / cur_size;
    };

    bool is_full() {
      return cur_size == buf_len;
    }

    ~RollingAverage() {
      free(buf);
    };

  private:
    int buf_len;
    float *buf;
    int next_idx;
    int cur_size;
};
