#define RELAY_4 12
#define MINUTES 60000

void setup() {
  // put your setup code here, to run once:
  pinMode(RELAY_4, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(RELAY_4, HIGH);
  delay(5 * MINUTES);
  digitalWrite(RELAY_4, LOW);
  delay(25 * MINUTES);
}
